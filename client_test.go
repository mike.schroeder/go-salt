package salt

import "testing"

func TestClient_GetArgs(t *testing.T) {
	c, _ := New()

	args := c.GetArgs([]string{"manage.present"})

	test := []string{"manage.present", "--out", "json"}

	for i := range test {
		if args[i] != test[i] {
			t.Error("Generated arguments are not expected args")
		}
	}

	c.Debug = true

	args = c.GetArgs([]string{"manage.present"})

	test = []string{"manage.present", "--out", "json", "test=true"}

	for i := range test {
		if args[i] != test[i] {
			t.Error("Generated arguments are not expected args")
		}
	}

}

func TestNew(t *testing.T) {
	c, _ := New()

	if c.Debug {
		t.Error("Debug should not be enabled by default")
	}

	if c.Salt != "salt" {
		t.Error("salt should be default command")
	}

	if c.SaltRun != "salt-run" {
		t.Error("salt-run shoud be default salt run command")
	}

	if c.Path != "/usr/bin" {
		t.Error("/usr/bin shoud be default path")
	}

	if c.Output != "json" {
		t.Error("json shoud be default output")
	}

	if c.DebugParam != "test=true" {
		t.Error("test=true should be default debug param")
	}

	setSalt := func(c *Client) {
		c.Salt = "not-salt"
	}

	c, _ = New(setSalt)

	if c.Salt != "not-salt" {
		t.Error("Failed to change value using variadric function")
	}

}

func TestClient_GetSaltPath(t *testing.T) {
	client, _ := New()

	if client.GetSaltPath() != "/usr/bin/salt" {
		t.Error("Incorrect salt executable path generated")
	}

}

func TestClient_GetSaltRunPath(t *testing.T) {
	client, _ := New()

	if client.GetSaltRunPath() != "/usr/bin/salt-run" {
		t.Error("Incorrect salt-run executable path generated")
	}
}

func TestClient_CallSalt(t *testing.T) {

}

func TestClient_CallSaltRun(t *testing.T) {

}
