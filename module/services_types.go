package module

type ServicesStatusRequest struct {
	Service   string
	Signature string
}
