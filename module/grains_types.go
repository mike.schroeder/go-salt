package module

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

type GrainsGetRequest struct {
	Key       string
	Default   string
	Delimiter string
	Ordered   bool
}

func (req *GrainsGetRequest) GetArgs() ([]string, error) {
	args := []string{}
	if req.Key == "" {
		return nil, errors.New("key is required for a grains.get request")
	}

	args = append(args, req.Key)

	if req.Default != "" {
		args = append(args, fmt.Sprintf("default='%s'", req.Default))
	}

	if req.Delimiter != "" {
		args = append(args, fmt.Sprintf("delimiter='%s'", req.Delimiter))
	}

	if req.Ordered != true {
		args = append(args, fmt.Sprintf("ordered=%s", strconv.FormatBool(req.Ordered)))
	}

	return args, nil
}

type GrainsGetResponse struct {
	Minion   string
	Response json.RawMessage
}
