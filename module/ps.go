package module

import (
	"gitlab.com/_mschroeder/go-salt"
)

type PSService interface {
	PSAux(service string) (map[string]interface{}, error)
}

type PSServiceImpl struct {
	Client *salt.Client
}

var _ PSService = &PSServiceImpl{}

func (p *PSServiceImpl) PSAux(service string) (map[string]interface{}, error) {
	args := []string{"ps.psaux"}
	args = append(args, service)

	var resp map[string]interface{}

	if err := p.Client.CallSalt(p.Client.Target, &resp, args...); err != nil {
		return nil, err
	}

	return resp, nil
}
